﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngLab.Site.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult First()
        {
            return PartialView();
        }

        public ActionResult Second()
        {
            return PartialView();
        }

        public ActionResult Third()
        {
            return PartialView();
        }
    }
}
