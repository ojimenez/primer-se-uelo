﻿function Router($routeProvider) {
    $routeProvider
    .when('/first', {
        templateUrl: '/Home/First',
        controller: 'FirstCtrl'
    })
    .when('/second', {
        templateUrl: '/Home/Second',
    })
    .otherwise({ redirectTo: '/index' });
}