﻿app.controller('FirstCtrl', ['$scope', function ($scope) {

    $scope.form =
    {
        email: '',
        name: ''
    };
    $scope.isRegistered = typeof labSession != "undefined";

    $scope.register = function() {
        RegisterSvc.Register({ Email: $scope.form.email, Name: $scope.form.name }).success(function (response) {
            if (response == 'ak7' || response == 'User already exists') {
                console.log('Go to the _Layout.cshtml, you need to uncomment the script that is reference to localhost:13096');
                console.log('In the src it has to reference to "http://52.24.213.64/AngLabRest/api/script/[YOUR_EMAIL]"');
                console.log('By example: if your register email was user@gmail.com "http://52.24.213.64/AngLabRest/api/script/user%40gmail"');
            }
        });
    }
    
}]);