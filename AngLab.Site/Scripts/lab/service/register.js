﻿function RegisterService($http) {
    delete $http.defaults.headers.common['X-Requested-With'];

    this.Register = function (data) {
        var promise = $http.post("http://52.24.213.64/AngLabRest/api/register/", JSON.stringify(data),
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        promise.success(function(response) {
            console.log(response);
        });

        return promise;
    };
}